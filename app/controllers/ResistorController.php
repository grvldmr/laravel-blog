<?php

class ResistorController extends BaseController {

	public function showIndex()
	{
		return View::make('resistor.index')
			->withTitle('Определение резистора')
			->withMetatext('Определение сопротивления резистора по цвету колец.')
			->withKeywords('резистор, сопротивление, цвета, кольца, определение, Ом');
	}

}
