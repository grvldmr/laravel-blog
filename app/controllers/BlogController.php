<?php


class BlogController extends BaseController {

    /**
     * Показывает индесную страницу
     * @return mixed
     */
	public function showIndex()
	{
		$blogs = Blog::all();
        return View::make('blog_index',array('blogs' => $blogs));
	}

    /**
     * Показывает детальную страницу
     * @return mixed
     */
    public function showDetail($id)
	{
        $blog = Blog::where('id', '=', $id)->first();
        if($blog == null)
        {
            App::abort(404);
        }
        $comments = $blog->comments()->get();
		return View::make('blog_detail',array('blog' => $blog, 'comments' => $comments));
	}
	
	/**
	 * Добавляет комментарий к блогу через аякс.
	 * @return mixed
	 */
	public function addComment($id)
	{
		if(Request::ajax())
		{
			$new = Input::get();
			$comment = new Comment($new);
			// attempt validation
			if ($comment->validate($new))
			{
				$blog = Blog::find($id);
				$comment = $blog->comments()->save($comment)->toArray();
				$comment["error"] = 0;
				return $comment;

			}
			else
			{
			    $errors = $comment->errors()->toArray();
			    $errors["error"] = 1	;
			    return $errors;
			}
			//return array($name,$comment);
		}
	}

}
