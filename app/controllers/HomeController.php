<?php

class HomeController extends BaseController {

	public function showIndex()
	{
		return View::make('main.index')
			->withTitle('Главная страница')
			->withMetatext('Определение сопротивления резистора по цвету колец.')
			->withKeywords('резистор, сопротивление, цвета, кольца, определение, Ом');
	}

}
