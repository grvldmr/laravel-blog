<?php


class UserController extends BaseController {

	public function getRegister() {
		return View::make('register');
	}

	public function postRegister() {
		$user - new User();
		if( !$user->validate( Input::get() ) ) {
			return Redirect::to("register")->withErrors($user->errors)->withInput();
		}

		$user->fill( Input::get() );
		$id = $user->register();

		return View::make('register');
	}

	public function postLogin() {
		$creds = Array(
			'password' => Input::get('password'),
			'login' =>  Input::get('login'),
			'isActive' => 1
		);

		if( Auth::attempt( $creds, Input::get('remember') ) ) {
			return Redirect::intend();
		}

		$error = "Не правильно.";

		return View::make('blogindex')->withError($error);
	}

	public function getLogout() {
		Auth::logout();
		return Redirect::to('/'); 
	}



}
 	