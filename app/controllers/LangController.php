<?php


class LangController extends BaseController {

	public function selectLang($lang = 'ru')
	{
		Session::put('lang',$lang);
		$redirect = Input::get('redirect');
		if(!empty($redirect))
			return Redirect::to($redirect);
		else
			return Redirect::route('home');
	}
}
