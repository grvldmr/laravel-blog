<?
class User extends Eloquent {   

    protected $fillable = array('login', 'password', 'email');

    private $rules = array(
        'login' => 'required|alpha_num|unique:users|min:3',
        'email'  => 'required|email|unique:users',
        'password'  => 'required|confirmed|min:6'
    );

    private $errors;

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    private function register() {
        $this->password = Hash::make($this->password);
        $this->activisationCode = $this->generateCode();
        $this->isActive = true;

        return $this->id;       
    }

    private function generateCode() {
        return Str::random();
    }

}
