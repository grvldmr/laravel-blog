<?
class Comment extends Eloquent {

    private $rules = array(
        'name' => 'required',
        'content'  => 'required'
    );

    private $errors;

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comments';

	protected $fillable = array('name', 'content');

    public function blog()
    {
        return $this->belongsTo('Blog');
    }

}
