<?
class Blog extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blogs';

    public function comments()
    {
        return $this->hasMany('Comment');
    }

//    public function scopePopular($query)
//    {
//        return $query->where('votes', '>', 100);
//    }

}
