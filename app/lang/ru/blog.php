<?php

return array(
	'title' => 'Простой блог',
	'writted' => 'Написано',
	'comment_count' => 'Комментариев',
	'no_comments' => 'Никто не оставлял комментариев',
	'add_comment' => 'Добавить комментарий',
	'send' => 'Отправить',
	'you_name' => 'Ваше имя',
	'you_comment' => 'Ваш комментарий'
);
