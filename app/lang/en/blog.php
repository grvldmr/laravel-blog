<?php

return array(
	'title' => 'Simple blog',
	'writted' => 'Writted',
	'comment_count' => 'Comments',
	'no_comments' => 'No comments =(',
	'add_comment' => 'Add comment',
	'send' => 'Send',	
	'you_name' => 'Your name',
	'you_comment' => 'Your comment'
);
