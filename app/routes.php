<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',array('as' => 'home', 'uses' => 'HomeController@showIndex'));

Route::get('/resistors',array('as' => 'resistor', 'uses' => 'ResistorController@showIndex'));

Route::get('/blog',array('as' => 'blog', 'uses' => 'BlogController@showIndex'));
Route::get('/blog/{id}','BlogController@showDetail');
Route::post('/blog/{id}','BlogController@addComment');

Route::get('/lang/{lang}',array('as' => 'select_lang', 'uses' => 'LangController@selectLang'));

Route::get('/admin','AdminController@showLogin');
Route::controller('/user','UserController');
