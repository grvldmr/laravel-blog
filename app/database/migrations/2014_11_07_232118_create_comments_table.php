<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments',function($table){
		    $table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->timestamps();
		    $table->string('name');
		    $table->text('content');
		    $table->integer('blog_id')->unsigned()->index();
		});
        DB::statement('ALTER TABLE blogs MODIFY COLUMN content TEXT');
	}
	

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('comments');
        DB::statement('ALTER TABLE blogs MODIFY COLUMN content VARCHAR(255)');
	}

}
                                              	