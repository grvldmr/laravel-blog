<!DOCTYPE html>
<html lang="<?=Config::get('app.locale');?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?=trans('blog.title');?>.</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/blog.css" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="header">
                <h1><a href="<?=URL::route('home');?>"><?=trans('blog.title');?></a></h1>
            </div>
            <hr>
            <a href="/lang/ru?redirect=<?=Route::currentRouteName();?>">RU</a>
            <a href="/lang/en?redirect=<?=Route::currentRouteName();?>">EN</a>
            <? if(Auth::check()): ?>
            <form action="<?=action("UserController@getLogout")?>" method="POST" name="form_login" id="form_login" role="form">
                 <?=Auth::user()->login;?>
                 <button class="btn btn-default" id="logout"><?=trans('user.logout');?></button>
            </form>
            <?else:?>
            <form action="<?=action("UserController@postLogin")?>" method="POST" name="form_login" id="form_login" role="form">
                <div class="form-group">
                    <label for="name"><?=trans('user.login');?></label>
                    <input type="text" name="login" id="login" maxlength="50" class="form-control" placeholder="<?=trans('user.login');?>">
                </div>
                <div class="form-group">
                    <label for="comment"><?=trans('user.password');?></label>
                    <input type="password" name="password" id="password" maxlength="120" class="form-control" placeholder="<?=trans('user.password');?>">
                </div>
                <button class="btn btn-default" id="enter"><?=trans('user.enter');?></button>
            </form>
            <?endif;?>

            <?foreach($blogs as $blog):?>
            <div class="row post">
                <h4>
                    <a href="/blog/<?=$blog->id;?>"><?=$blog->title;?></a>
                </h4>
                <p>
                    <?=substr($blog->content,0,600);?>...
                </p>
                <div class="well well-sm">
                    <span><?=trans('blog.writted');?>:&nbsp;<?=$blog->created_at;?></span>
                    <span class="right"><?=trans('blog.comment_count');?>:&nbsp;<?=$blog->comments()->count();?></span>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </body>
</html>
