@extends('main/layout')

@section('content')
<main>
      <h2>
            Здесь пока ничего нету.
      </h2>
      <div class="resistor">
            Но если вам нужно определить резистор по рисунку
            то посмотрите {{ link_to_route('resistor', 'тут') }}.
      </div>
</main>
@stop