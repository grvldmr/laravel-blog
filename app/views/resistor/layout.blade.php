<!DOCTYPE html>
<html lang="<?=Config::get('app.locale');?>">
      <head>
            <meta name="description" content="{{ $metatext }}"/>
            <meta name="keywords" content="{{ $keywords }}"/>                        
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>2chance.tk | {{ $title }}</title>
            <link href="/assets/css/resistor.css" rel="stylesheet">

            <script src="/assets/js/angular.min.js" type="text/javascript"></script>

            <!--[if lt IE 9]>
                  <script src="/js/html5shiv.js"></script>
            <![endif]-->
            <link rel="shortcut icon" type='image/x-icon' href="/resistor.ico">
      </head>
      <body>
            @yield('content')
      </body>
</html>
