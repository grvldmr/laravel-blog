@extends('resistor/layout')

@section('content')
<main ng-app="resistor" ng-controller="MainController" ng-cloak>
	<h2>Резистор - кун</h2>
	<div class="rings_n">
		<p>Число колец на резисторе</p>
		<label><input type="radio" ng-model="rings" ng-init="fourRings = true" name="ring4" value="4" ng-change='change()' checked="checked">4 кольца</label>
		<label><input type="radio" ng-model="rings" ng-init="fiveRings = false" name="ring5" ng-change='change()' value="5">5 колец</label>
	</div>

	<div class="colors">
		<p>Цвета колец слева направо, начиная с самого широкого кольца</p>
		<form>
			<select ng-model='firstColor' ng-change='change()'>
				<option ng-repeat="color in firstColorsList" value="{[{color.id }]}">{[{ color.name }]}</option>
			</select>
			<select ng-model='secondColor' ng-change='change()'>
				<option ng-repeat="color in colorsList" value="{[{color.id }]}">{[{ color.name }]}</option>
			</select>
			<select ng-model='thirdColor' ng-show='rings == 5' ng-change='change()'>
				<option ng-repeat="color in colorsList" value="{[{color.id }]}">{[{ color.name }]}</option>
			</select>
			<select ng-model='fourthColor' ng-change='change()'>
				<option ng-repeat="color in coefficientList" value="{[{color.id }]}">{[{ color.name }]}</option>
			</select>
			<select ng-model='fifthColor' ng-change='change()'>
				<option ng-repeat="precision in precisionList" value="{[{precision.id }]}">{[{ precision.name }]}</option>
			</select>
		</form>
	</div>
	<div class="result">
		<p>Результат</p>
		<form>
			<input type="text" ng-model='result' disabled>
		</form>

	</div>
	<script src="/assets/js/resistor/app.js" type="text/javascript"></script>
	<script src="/assets/js/resistor/controllers/main.js" type="text/javascript"></script>
</main>
@stop