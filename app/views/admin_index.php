<!DOCTYPE html>
<html lang="ru" ng-app="admin">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Админка блога.</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/blog.css" rel="stylesheet">
        <script src="/js/angular.min.js" type="text/javascript"></script>
        <script src="/js/angular-ui-router.min.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
            <script src="/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body ng-controller="MainController" >
      <div ui-view>
      </div>
<!--       <script src="/js/admin/config.js" type="text/javascript"></script>-->
      <script src="/js/admin/app.js" type="text/javascript"></script>
      <script src="/js/admin/services/auth.js" type="text/javascript"></script>
      <script src="/js/admin/controllers/main.js" type="text/javascript"></script>
      <script src="/js/admin/run.js" type="text/javascript"></script>
    </body>



















        <script type="text/javascript">
            // var adminApp = angular.module('adminApp', [
            //   'ngRoute',
            //   'authorizationModule',
            //   'securityModule'
            // ]);


            // adminApp.config(['$routeProvider',
            //   function($routeProvider) {
            //     $routeProvider.
            //       // when('/', {
            //       //   templateUrl: '/templates/main.html',
            //       //   controller: 'MainController'
            //       // }).
            //       when('/login', {
            //         templateUrl: '/templates/login.html',
            //         controller: 'LoginController'
            //         access: {
            //           login: true,
                      
            //         }
            //       }).
            //       when('/blog/:blogId?', {
            //         templateUrl: '/templates/blogs.html',
            //         controller: 'BlogController'
            //       }).
            //       otherwise({
            //         redirectTo: '/login'
            //       });
            //   }]);

            // var authorizationModule = angular.module('authorizationModule', []);
             
            // authorizationModule.controller('LoginController', ['$scope', 'authorizationFactory', '$location', 
            // function($scope, authorizationFactory, $location){
            //   $scope.loginClick = function() {
            //     if (authorizationFactory.login($scope.login, $scope.password)) {
            //       $location.path('/blog');
            //     } else {
            //       alert('Pass is 123456!');
            //     }
            //   }
            // }]);
             
            // authorizationModule.factory('authorizationFactory',['$userProvider',
            //   function($userProvider){
            //     var login = function(login, password){
            //       if (password !== '123456') {
            //         return false;
            //       }
            //       if (login === 'admin') {
            //         $userProvider.setUser({Login: login, Roles: [$userProvider.rolesEnum.Admin]});
            //       } else {
            //         $userProvider.setUser({Login: login, Roles: [$userProvider.rolesEnum.User]});
            //       }
            //       return true;
            //     }
             
            //     return {
            //       login: login,
            //     }
            // }]);
             
            // authorizationModule.factory('$userProvider', function(){
            //   var rolesEnum = {
            //     Admin: 0,
            //     User: 1
            //   };
            //   var setUser = function(u){
            //     user = u;
            //   }
            //   var getUser = function(){
            //     return user;
            //   }
             
            //   return {
            //     getUser: getUser,
            //     setUser: setUser,
            //     rolesEnum: rolesEnum
            //   }
            // });

            // angular.module('securityModule', ['authorizationModule'])
            // .factory('$pagesSecurityService', ['$userProvider', '$location',
            //     function ($userProvider, $location) {
             
            //         var checkAuthorize = function(path) {
            //             if ($userProvider.getUser() == null) {
            //                 $location.path('/login');
            //             }
            //             switch (path) {
            //               //запрещенный ресурс
            //               case '/blog':
            //                   return checkPageSecurity({
            //                       //роли текущего пользователя
            //                       UserRoles: $userProvider.getUser().Roles,
            //                       //роли, которым доступен ресурс
            //                       AvailableRoles: [
            //                           $userProvider.rolesEnum.Admin
            //                       ]
            //                   });
            //             default:
            //                 return true;
            //             }
            //         };
             
            //         var checkPageSecurity = function (config) {
            //             var authorize = false;
            //             for (var i in config.UserRoles) {
            //                 if ($.inArray(config.UserRoles[i], config.AvailableRoles) == -1) {
            //                     authorize = false;
            //                 } else {
            //                     authorize = true;
            //                     break;
            //                 }
            //             }
            //             return authorize;
            //         };
             
            //         return {
            //             checkAuthorize: checkAuthorize,
            //         };
            //     }]);

            // adminApp.controller('ApplicationController', ['$scope', '$location', '$pagesSecurityService', '$userProvider',
            //   function($scope, $location, $pagesSecurityService, $userProvider){
            //     $scope.goTo = function(path){
            //       $location.path(path);
            //     }
            //     angular.extend($scope, $userProvider, true);
                
            //     //контроль доступа
            //     $scope.$on('$locationChangeStart', function (event, nextUrl, prevUrl) {
            //         if ($location.path() != '/login' || nextUrl.indexOf('login') == -1) {
            //             if (!$pagesSecurityService.checkAuthorize($location.path())) {
            //                 alert('Access denied!');
            //                 $location.path(prevUrl.split('#')[1]);
            //             }
            //         }
            //     });
            // }]);

            // adminApp.controller('MainController', function ($scope) {

            // });

            // adminApp.controller('BlogController', function ($scope, $routeParams) {

            // });










            // adminApp.constant('AUTH_EVENTS', {
            //     loginSuccess: 'auth-login-success',
            //     loginFailed: 'auth-login-failed',
            //     logoutSuccess: 'auth-logout-success',
            //     sessionTimeout: 'auth-session-timeout',
            //     notAuthenticated: 'auth-not-authenticated',
            //     notAuthorize: 'auth-not-authorized'
            // });

            // adminApp.constant('USER_ROLES', {
            //     all: '*',
            //     admin: 'admin'
            // });

            // adminApp.factory('AuthService', function ($http, Session) {
            //     var authService = {};

            //     authService.login = function (user) {
            //         return $http
            //             .post('/admin/login', user)
            //             .then(function (res) {
            //                 Session.create(res.data.id, res.data.user.id,
            //                     res.data.user.role);
            //                 return res.data.user;
            //             });
            //     };

            //     authService.isAuthenticated = function () {
            //         return !!Session.userId;
            //     };

            //     authservice.isAuthorized = function (authorizedRoles) {
            //         if(!angular.isArray(authorizedRoles)) {
            //             authorizedRoles = [authorizedRoles];;
            //         }

            //         return (authService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) !== -1);
            //     };

            //     return authService;
            // });

            // adminApp.service('Session', function () {
            //     this.create = function (sessionId, userId, userRole) {
            //         this.id = sessionId;
            //         this.userId = userId;
            //         this.userRole = userRole;
            //     };
            //     this.destroy = function () {
            //         this.id = null;
            //         this.userId = null;
            //         this.userRole = null;
            //     };
            //     return this;
            // });







        </script>


</html>
