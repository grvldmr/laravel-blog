<!DOCTYPE html>
<html lang="<?=Config::get('app.locale');?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?=trans('blog.title');?>.</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/blog.css" rel="stylesheet">
        <script src="/js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="/js/handlebars-v2.0.0.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
            <script src="/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="header">
                <h1><a href="/"><?=trans('blog.title');?></a></h1>
            </div>
            <hr>
            <a href="/lang/ru?redirect=/<?=Request::path();?>">RU</a>
            <a href="/lang/en?redirect=/<?=Request::path();?>">EN</a>
            <? if(Auth::check()): ?>
            <form action="<?=action("UserController@getLogout")?>" method="POST" name="form_login" id="form_login" role="form">
                 <?=Auth::user()->login;?>
                 <button class="btn btn-default" id="logout"><?=trans('user.logout');?></button>
            </form>
            <?else:?>
            <form action="<?=action("UserController@postLogin")?>" method="POST" name="form_login" id="form_login" role="form">
                <div class="form-group">
                    <label for="name"><?=trans('user.login');?></label>
                    <input type="text" name="login" id="login" maxlength="50" class="form-control" placeholder="<?=trans('user.login');?>">
                </div>
                <div class="form-group">
                    <label for="comment"><?=trans('user.password');?></label>
                    <input type="password" name="password" id="password" maxlength="120" class="form-control" placeholder="<?=trans('user.password');?>">
                </div>
                <button class="btn btn-default" id="enter"><?=trans('user.enter');?></button>
            </form>
            <?endif;?>

            <div class="row">
                <h4>
                    <?=$blog->title;?>
                </h4>
                <p>
                    <?=$blog->content;?>
                </p>
                <div class="well well-sm">
                    <span><?=trans('blog.writted');?>:&nbsp;<?=date("d F Y",strtotime($blog->created_at));?></span>
                    <span class="right"><?=trans('blog.comment_count')?>:&nbsp;<?=$blog->comments()->count();?></span>
                </div>
            </div>
            <div class="row">
                <?if($comments->isEmpty()):?>
                <div class="no-comments"><?=trans('blog.no_comments')?></div>
                <?else:?>
                <div class="col-md-12 comments">
                    <?foreach($comments as $comment):?>
                    <div class="comment">
                        <div class="comment-head">
                            <h5>
                                <?=$comment->name;?>
                            </h5>
                            <span>
                                <?=date("d F Y",strtotime($comment->created_at));?>
                            </span>
                        </div>
                        <div class="comment-content">
                            <p>
                                <?=$comment->content;?>
                            </p>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
                <?endif;?>
                <div class="add_comment" id="add_comment"><?=trans('blog.add_comment');?></div>
                <div class="message_block"></div>
                <form action="" method="POST" name="form_comment" id="form_comment" role="form">
                    <div class="form-group">
                        <label for="name"><?=trans('blog.you_name');?></label>
                        <input type="text" name="name" id="name" maxlength="50" class="form-control" placeholder="<?=trans('blog.you_name');?>...">
                    </div>
                    <div class="form-group">
                        <label for="comment"><?=trans('blog.you_comment');?></label>
                        <textarea name="content" id="content" cols="30" rows="10" maxlength="120" class="form-control" placeholder="<?=trans('blog.you_comment');?>..."></textarea>
                    </div>
                    <button class="btn btn-default" id="submit"><?=trans('blog.send');?></button>
                </form>
            </div>
        </div>
    </body>
    <script id="comment-template" type="text/x-handlebars-template">
        <div class="comment">
            <div class="comment-head">
                <h5>
                    {{ name }}
                </h5>
                <span>
                    {{ created_at }}
                </span>
            </div>
            <div class="comment-content">
                <p>
                    {{ content }}
                </p>
            </div>
        </div>
    </script>    
    <script id="message-template" type="text/x-handlebars-template">
        <div class="{{ templclass }}">
            {{ message }}
            {{ name }}
            {{ content }}
        </div>
    </script>    

    <script type="text/javascript">
        $('#add_comment').on('click', function(){
            $("form#form_comment").slideDown();
        });
        $('#submit').on('click', function(event){
            event.preventDefault();
            $.ajax({
                    url: '',
                    type: "POST",
                    data: $("form#form_comment").serialize(),
                    success: function($data) {
                        if($data.error==0) {
                            var source   = $("#comment-template").html();
                            var template = Handlebars.compile(source);
                            var context = {name: $data.name, created_at: $data.created_at, content: $data.content}
                            var html    = template(context);
                            $(".comments").append(html);
                            $("input").val("");
                            $("textarea").val("");
                        } else {
                            console.log($data);
                            var source   = $("#message-template").html();
                            var template = Handlebars.compile(source);
                            var context = {name: $data.name, content: $data.content,  templclass: "error"}
                            var html    = template(context);
                            console.log(html);
                            $("div.message_block").append(html);
                        }                       
                    },
                    error: function($data) {
                        var source   = $("#message-template").html();
                        var template = Handlebars.compile(source);
                        var context = {name: "<?=trans('blog.server-error');?>", templclass: "error"}
                        var html    = template(context);
                        $(".message_block").append(html);
                    }
                });
        });
    </script>
</html>
