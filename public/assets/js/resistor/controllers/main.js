"use strict";

resistor.controller('MainController', function ($scope){

  $scope.firstColorsList = [
	{'name':'Коричневый','id': '1','color': '464646'},
	{'name':'Красный','id': '2','color': '740307'},
	{'name':'Оранжевый','id': '3','color': 'FF7810'},
	{'name':'Жёлтый','id': '4','color': 'FFD60B'},
	{'name':'Зелёный','id': '5','color': '237000'},
	{'name':'Голубой','id': '6','color': '04005E'},
	{'name':'Фиолетовый','id': '7','color': '712BB0'},
	{'name':'Серый','id': '8','color': 'aaa'},
	{'name':'Белый','id': '9','color': 'fff'}
  ];

  $scope.colorsList = [
	{'name':'Чёрный','id': '0','color': '000'},
	{'name':'Коричневый','id': '1','color': '464646'},
	{'name':'Красный','id': '2','color': '740307'},
	{'name':'Оранжевый','id': '3','color': 'FF7810'},
	{'name':'Жёлтый','id': '4','color': 'FFD60B'},
	{'name':'Зелёный','id': '5','color': '237000'},
	{'name':'Голубой','id': '6','color': '04005E'},
	{'name':'Фиолетовый','id': '7','color': '712BB0'},
	{'name':'Серый','id': '8','color': 'aaa'},
	{'name':'Белый','id': '9','color': 'fff'}
  ];
  
  $scope.coefficientList = [
    {'name':'Серебряный','id': '0','color': 'EAEAEA','coeff': '0.01'},
	{'name':'Золотой','id': '1','color': 'E9DB85','coeff': '0.1'},
	{'name':'Чёрный','id': '2','color': '000','coeff': '1'},
	{'name':'Коричневый','id': '3','color': '464646','coeff': '10'},
	{'name':'Красный','id': '4','color': '740307','coeff': '100'},
	{'name':'Оранжевый','id': '5','color': 'FF7810','coeff': '1000'},
	{'name':'Жёлтый','id': '6','color': 'FFD60B','coeff': '10000'},
	{'name':'Зелёный','id': '7','color': '237000','coeff': '100000'},
	{'name':'Голубой','id': '8','color': '04005E','coeff': '1000000'},
	{'name':'Фиолетовый','id': '9','color': '712BB0','coeff': '10000000'},
	{'name':'Серый','id': '10','color': 'aaa','coeff': '100000000'},
	{'name':'Белый','id': '11','color': 'fff','coeff': '1000000000'}
  ];

  $scope.precisionList = [
	{'name':'Серебряный','id': '0', 'precision':'10','color': 'EAEAEA'},
	{'name':'Золотой','id': '1', 'precision':'5','color': 'E9DB85'},
	{'name':'Коричневый','id': '2', 'precision':'1','color': '464646'},
	{'name':'Красный','id': '3', 'precision':'2','color': '740307'},
	{'name':'Зелёный','id': '4', 'precision':'0.5,','color': '237000'},
	{'name':'Голубой','id': '5', 'precision':'0.25','color': '04005E'},
	{'name':'Фиолетовый','id': '6', 'precision':'0.1','color': '712BB0'},
	{'name':'Серый','id': '7', 'precision':'0.05','color': 'aaa'},
  ];

  $scope.rings = '4';

  $scope.change = function() {
	switch ($scope.rings) {
	   case '4':
	   	  $scope.result = '';
	   	  if(!angular.isUndefined($scope.firstColor) &&
	   	  	!angular.isUndefined($scope.secondColor) &&
	   	  	//!angular.isUndefined($scope.thirdColor) &&
	   	  	!angular.isUndefined($scope.fourthColor) &&
	   	  	!angular.isUndefined($scope.fifthColor) ) {
	   	  	var string = '';
	   	  	string += parseInt(''+$scope.firstColor+$scope.secondColor)*$scope.coefficientList[$scope.fourthColor].coeff
	   	  	string += "Ом +-"+$scope.precisionList[$scope.fifthColor].precision+"%";
	   	  	$scope.result = string;
	   	  }
	      break
	   case '5':
	   	  $scope.result = '';
	   	  if(!angular.isUndefined($scope.firstColor) &&
	   	  	!angular.isUndefined($scope.secondColor) &&
	   	  	!angular.isUndefined($scope.thirdColor) &&
	   	  	!angular.isUndefined($scope.fourthColor) &&
	   	  	!angular.isUndefined($scope.fifthColor) ) {
	   	  	var string = '';
	   	  	string += parseInt(''+$scope.firstColor+$scope.secondColor+$scope.thirdColor)*$scope.coefficientList[$scope.fourthColor].coeff
	   	  	string += "Ом +-"+$scope.precisionList[$scope.fifthColor].precision+"%";
	   	  	$scope.result = string;
	   	  }
	      break
	   default:
	      $scope.result = '';
	      break
	}
  }

});