"use strict";

var resistor = angular
  .module('resistor', [])
  .config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  })
  .run(function() {

  });