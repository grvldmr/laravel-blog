angular.module('admin')
  .service('auth', function auth($q, $http){

    authenticatedUser = null;

    return {

      requestUser : function () {
        
        var deferred = $q.defer;

        $http.get('api/user.json').success(function(user){
          authenticatedUser = user;
          deferred.resolve(user);
        }).error(function(error){
          deferred.reject(error);
        })

        return deferred.promise;
      },

      getUser : function () {
        return authenticatedUser;
      },

      exists : function () {
        return authenticatedUser != null;
      },

      login : function (creds) {
        var deferred = $q.defer;

        $http.post('/auth/login', creds).success(function(user){
          if(user) {
            authenticatedUser = user;
            deferred.resolve(user);            
          }
          else {
            deferred.reject('error icorrect');  
          }
         
        }).error(function(error){
            deferred.reject(error);  
        });

        return deferred.promise;
      },

      logout : function () {
        authenticatedUser = null;
      },

      isAdmin : function () {
        return $this.exists() && authenticatedUser.isAdmin == 'admin';
      }
    }
  })