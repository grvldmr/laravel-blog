"use strict";

angular
  .module('admin', [
    'ui-route'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
    .state('main', {
        url: "/",
        templateUrl: "/templates/main.html",
        controller: MainController,
        authenticate: false
      })  
      .state('login', {
        url: "/login",
        templateUrl: "/templates/login.html",
        controller: LoginController,
        authenticate: false
      })
      .state('blog', {
        url: "/blog",
        templateUrl: "/templates/blog.html",
      controller: BlogController,
      authenticate: true
      })
      .state('blog.edit', {
        url: "/blog/:id",
        templateUrl: "/templates/blog.html",
        controller: BlogController,
        authenticate: true
      })
      .state('register', {
        url: "/register",
        templateUrl: "/templates/register.html",
        controller: RegisterController,
        authenticate: false
      });
  })
  .run(function(auth) {
    auth.requestUser();
  });